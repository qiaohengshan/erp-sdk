<?php
namespace joyqhs\Sdk\winit;

class callApiUtil
{
    const PLATFORM = 'zhimu';
    const VERSION = '1.0';
    const SIGN_METHOD = 'md5';
    const FORMAT = 'json';
    const CONNECT_TIMEOUT = 120;
    const READ_TIMEOUT = 120;
    const SERVICE_URL = 'https://openapi.winit.com.cn/openapi/service';

    public static function callWinitApi($action, $data)
    {
        ksort($data);
        $data = json_encode($data);
        $postData = self::buildSignData($action, $data);
        $postData = array_merge($postData, self::getSystemParams(), ['client_id' => config('erp_sdk.winit.clientid')]);
        //请求接口获取数据
        ksort($postData);
        $postData['data'] = json_decode($postData['data'], true);
        $response = self::postCurl(self::SERVICE_URL, json_encode($postData));
        return json_decode($response, true);
    }


    public static function postCurl($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (self::CONNECT_TIMEOUT) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECT_TIMEOUT);
        }
        if (self::READ_TIMEOUT) {
            curl_setopt($ch, CURLOPT_TIMEOUT, self::READ_TIMEOUT);
        }
        $response = curl_exec($ch);
        unset($data);
        try {
            if (curl_errno($ch)) {
                throw new \Exception(curl_error($ch), 0);
            } else {
                $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($httpStatusCode !== 200) {
                    throw new \Exception($response, $httpStatusCode);
                }
            }
        } finally {
            curl_close($ch);
        }

        return $response;
    }


    public static function buildSignData($action, $data) : array
    {
        $postData = [
            'action' => $action,
            'data' => $data
        ];
        $systemParam = self::getSystemParams();
        $postData = array_merge($systemParam, $postData);
        list($postData['sign'], $postData['client_sign']) = [
            self::generateSign($postData),
            self::generateSign($postData, 'client')
        ];
        return $postData;
    }



    private static function getSystemParams() : array
    {
        return [
            'app_key' => config('erp_sdk.winit.appkey'),
            'format' => self::FORMAT,
            'platform' => self::PLATFORM,
            'sign_method' => self::SIGN_METHOD,
            'timestamp' => self::getTimestamp(),
            'version' => self::VERSION
        ];
    }


    private static function getTimestamp()
    {
        return date('Y-m-d H:i:s');
    }


    private static function generateSign($params, $type = 'account') : string
    {
        ksort($params);
        $stringToBeSigned = ('account' == $type) ? config('erp_sdk.winit.token'): config('erp_sdk.winit.secret');
        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }
        unset($k, $v);
        $stringToBeSigned .= ('account' == $type) ? config('erp_sdk.winit.token') : config('erp_sdk.winit.secret');
        return strtoupper(md5($stringToBeSigned));
    }

}
