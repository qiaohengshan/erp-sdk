<?php

namespace  joyqhs\Sdk\ebay;

interface PlatformApiInterface
{

    /**
     *  设置请求参数
     */
    public function setRequest();


    /**
     *  获取请求
     */
    public function getRequest();


    /**
     *  发送请求
     */
    public function sendRequest();


    /**
     *  获取响应信息
     */
    public function getResponse();

}

