<?php
/**
 *  获取账号下的帐单信息
 * @author arke.wu
 * @since 2018-10-20
 */

namespace joyqhs\Sdk\ebay;

use joyqhs\Sdk\ebay\EbayApiAbstract;
use joyqhs\Sdk\ebay\XmlGenerator;

class GetAccountRequest extends EbayApiAbstract {

    /** @var 接口名 */
    public $_verb = 'GetAccount';

    /**@var string 每次请求订单个数 */
    public $_EntriesPerPage = 100;

    /**@var string 请求的页数 */
    public $_PageNumber = 1;

    /** @var string item id */
    protected $_itemID = null;

    /** @var string  AccountEntrySortType default value: AccountEntryFeeTypeAscending*/
    protected $_AccountEntrySortType = '';

    /** @var string AccountHistorySelection  */
    protected $_AccountHistorySelection = '';

    /** @var string 帐单日期 */
    protected $_InvoiceDate = '';

    /** @var string 自定义返回的结果 */
    protected $_OutputSelector = '';

    
    public function setRequest()
    {
        $request = array(
            'RequesterCredentials' => array(
                'eBayAuthToken' => $this->getToken(),
            ),
            'Pagination' => array(
                'EntriesPerPage' => $this->_EntriesPerPage,
                'PageNumber' => $this->_PageNumber,
            )
        );

        if (!is_null($this->_itemID)) {
            $request['ItemID'] = $this->_itemID;
        }
        if (!empty($this->_AccountEntrySortType)) {
            $request['AccountEntrySortType'] = $this->_AccountEntrySortType;
        }
        if (!empty($this->_AccountHistorySelection)) {
            $request['AccountHistorySelection'] = $this->_AccountHistorySelection;
        }
        if (!empty($this->_BeginDate)) {
            $request['BeginDate'] = $this->_BeginDate;
        }
        if (!empty($this->_EndDate)) {
            $request['EndDate'] = $this->_EndDate;
        }
        if (!empty($this->_InvoiceDate)) {
            $request['InvoiceDate'] = $this->_InvoiceDate;
        }
        if (!empty($this->_OutputSelector)) {
            $request[] = $this->_OutputSelector;
        }

        $this->request = $request;
        return $this;
    }


    public function setItemID($itemID)
    {
        $this->_itemID = $itemID;
        return $this;
    }

    public function setAccountEntrySortType($sortType)
    {
        $this->_AccountEntrySortType = $sortType;
        return $this;
    }

    public function setAccountHistorySelection($selection)
    {
        $this->_AccountHistorySelection = $selection;
        return $this;
    }


    public function setBeginDate($beginDate)
    {
        $this->_BeginDate = $beginDate;
        return $this;
    }


    public function setEndDate($endDate)
    {
        $this->_EndDate = $endDate;
        return $this;
    }

    public function setInvoiceDate($InvoiceDate)
    {
        $this->_InvoiceDate = $InvoiceDate;
        return $this;
    }

    public function setPageNum($pageNum)
    {
        $this->_PageNumber = $pageNum;
        return $this;
    }


    public function setOutputSelector(array $outputSelectors)
    {
        $xmlGeneration = new XmlGenerator();//Xml生成器
        $this->_OutputSelector = $xmlGeneration->buildXMLFilter($outputSelectors, 'OutputSelector')->pop()->getXml();
        return $this;
    }

}