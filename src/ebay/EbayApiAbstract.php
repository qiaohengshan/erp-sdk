<?php
/**
 * Created by PhpStorm.
 * User: arkewu
 * Date: 2018/6/25
 * Time: 17:08
 *
 * eBay 抽像类，各方法都继承此抽象类，调用此方法
 */


namespace joyqhs\Sdk\ebay;

abstract class EbayApiAbstract implements PlatformApiInterface
{

    /**@var string 用户Token */
    protected $_usertoken = null;

    /**@var string 站点ID */
    protected $_siteID = 0;//某些不需要传站点的请求默认为美国站

    /**@var string 开发者账号ID(与开发者账号绑定) */
    protected $_devID = null;

    /**@var string APP ID(与开发者账号绑定) */
    protected $_appID = null;

    /**@var string 证书ID(与开发者账号绑定) */
    protected $_certID = null;

    /**@var string 请求接口名 */
    protected $_verb = null;

    /**@var string 请求版本号 */
    protected $_compatLevel = null;

    /**@var string 服务地址 */
    protected $_serverUrl = null;

    /**@var string 交互类型 */
    protected $_callType = 'trading';

    /**@var string 添加文件传输表头 */
    protected $_boundary = '';

    /**@var int 账号ID */
    protected $accountID = 0;

    /**@var string 请求内容 */
    protected $request = null;

    /**@var string 返回响应信息 */
    protected $response = null;

    protected $_requestbody = null;

    /**@var string 异常信息 */
    protected $errorMessage = null;

    protected $_xmlsn = 'urn:ebay:apis:eBLBaseComponents';

    protected $_isXML = 0;/* 是否返回xml字符串*/
    /**
     *  设置账号信息
     * @param int $accountID
     */
    public function setAccount($account)
    {
        $this->accountID = $account['account_id'];
        $this->_usertoken = $account['user_token'];
        $this->_appID = $account['app_id'];
        $this->_devID = $account['dev_id'];
        $this->_certID = $account['cert_id'];
        $this->_serverUrl = $account['server_url'];
        $this->_compatLevel = $account['compat_level'];
        return $this;
    }

    /**
     *  设置站点ID
     * @param number $siteID
     */
    public function setSiteID($siteID = 0)
    {
        $this->_siteID = $siteID;
        return $this;
    }

    /**
     *  设置是否xml格式，0表示对象，1表示字符串
     * @param unknown $xml
     * @return EbayApiAbstract
     */
    public function setIsXML($xml)
    {
        $this->_isXML = $xml;
        return $this;
    }

    /**
     *  设置交互接口
     * @param string $verb
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;
        return $this;
    }

    /**
     * @param string $message
     * @return $this
     *  设置报错信息
     */
    public function setExceptionMessage($message)
    {
        $this->errorMessage = $message;
        return $this;
    }



    /**
     *  发送请求,获取响应结果
     */
    public function sendRequest()
    {
        try {
            $ebayAPI = new EbaySession($this->_usertoken, $this->_devID, $this->_devID, $this->_certID, $this->_serverUrl, $this->_compatLevel, $this->_siteID, $this->_verb, $this->_callType, $this->_boundary);
            $requestXmlBody = $this->_requestbody = $this->getRequestXmlBody();
            $response = $ebayAPI->sendHttpRequest($requestXmlBody, $this->_isXML);
            $this->response = $response;
            if (!$this->getIfSuccess()) {
                $errorMessage = '';
                if (isset($this->response->Errors)) {
                    foreach ($this->response->Errors as $error) {
                        $errorMessage .= (string)$error->LongMessage . ".";
                    }
                }
                $this->setExceptionMessage($errorMessage);
            }
        } catch (Exception $e) {
            $this->setExceptionMessage($e->getMessage());
        }
        return $this;
    }

    /**
     *  将请求参数转化为Xml
     */
    public function getRequestXmlBody()
    {
        $xmlGeneration = new XmlGenerator();
        return $xmlGeneration->XmlWriter()->push($this->getXmlRequestHeader(), array('xmlns' => $this->_xmlsn))
            ->buildXMLFilter($this->getRequest())
            ->pop()
            ->getXml();
    }

    /**
     *  获取请求参数
     * @see ApiInterface::getRequest()
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *  获取响应结果
     * @see ApiInterface::getResponse()
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *  获取token
     * @return string
     */
    public function getToken()
    {
        return $this->_usertoken;
    }

    /**
     *  判断交互是否成功
     */
    public function getIfSuccess()
    {
        if (isset($this->response->Ack) && ($this->response->Ack == 'Success' || $this->response->Ack == 'Warning')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *  获取xml请求头名称
     */
    public function getXmlRequestHeader()
    {
        return $this->_verb . 'Request';
    }

    /**
     *  获取失败信息
     * @return string
     */
    public function getErrorMsg()
    {
        return $this->errorMessage;
    }


    public function setXmlsn($xmlsn)
    {
        $this->_xmlsn = $xmlsn;
        return $this;
    }

}

?>