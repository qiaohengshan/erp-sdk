<?php

namespace joyqhs\Sdk\ebay;

use joyqhs\Sdk\ebay\EbayApiAbstract;
use joyqhs\Sdk\ebay\XmlGenerator;

class GetOrderTransactionsRequest extends EbayApiAbstract
{

    /**@var string 平台订单号 */
    public $_OrderIDArray = null;

    /**@var string 包含手续费 */
    public $_IncludeFinalValueFee = 'true';

    public $_verb = 'GetOrderTransactions';



    /**
     * @param array $orders
     * @return $this
     *  设置订单号
     */
    public function setOrderIDArray(array $orders)
    {
        $xmlGeneration = new XmlGenerator();//Xml生成器
        $this->_OrderIDArray = $xmlGeneration->buildXMLFilter($orders, 'OrderID')->pop()->getXml();
        return $this;
    }


    /**
     *  设置请求参数
     * @see PlatformApiInterface::setRequest()
     */
    public function setRequest()
    {
        $request = array(
            'RequesterCredentials' => array(
                'eBayAuthToken' => $this->getToken(),
            ),
            'IncludeFinalValueFees' => $this->_IncludeFinalValueFee,
        );
        if (!empty($this->_OrderIDArray)) {
            $request['OrderIDArray'] = $this->_OrderIDArray;
        }
        $this->request = $request;
        return $this;
    }

}