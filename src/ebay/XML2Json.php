<?php
/**
 * Created by PhpStorm.
 * User: arke.wu
 * Date: 2018/6/29
 * Time: 17:44
 */

namespace  joyqhs\Sdk\ebay;

class XML2Json
{
    public static function xml2json($source)
    {
        /*
        //传的是文件，还是xml的string的判断
        if (is_file($source)) {
            $xml_array = simplexml_load_file($source);
        } else {
            $xml_array = simplexml_load_string($source);
        }
        */
        $xml_array = simplexml_load_string($source);
        $json = json_encode($xml_array);
        return $json;
    }


    public static function json2array($json)
    {
        if (!empty($json)) {
            return json_decode($json, true);
        }
        return [];
    }



}