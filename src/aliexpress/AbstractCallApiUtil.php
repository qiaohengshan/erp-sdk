<?php

namespace joyqhs\Sdk\aliexpress;

abstract class AbstractCallApiUtil
{
    protected static $instance;

    protected $connectTimeout = 120;

    protected $readTimeout = 120;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new static();
        }
        return self::$instance;
    }


    final public function postCurl($url, array $data)
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false
        ));
        if ($this->connectTimeout) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        }
        if ($this->readTimeout) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->connectTimeout);
        }
        $response = curl_exec($ch);
        unset($data);
        try {
            if (curl_errno($ch)) {
                throw new Exception(curl_error($ch), 0);
            } else {
                $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($httpStatusCode !== 200) {
                    throw new Exception($response, $httpStatusCode);
                }
            }
        } finally {
            curl_close($ch);
        }
        return $response;
    }


    public function responseToArray($response)
    {
        return json_decode($response, true);
    }


    final public function assembleAliexpressParams($method, $sessionKey, $params)
    {
        $assembleParams = array(
            'session_key' => $sessionKey,
            'method' => $method,
            'token' => $this->getToken($method)
        );
        $assembleParams = $assembleParams + $params;
        return $assembleParams;
    }


    final protected function getToken($method)
    {
        return md5($method . '-api-' . '479fe803d70b9a45100c976628810b27c908fdde');
    }


    abstract public function callAliexpressApi($method, $sessionKey, $params);
}
