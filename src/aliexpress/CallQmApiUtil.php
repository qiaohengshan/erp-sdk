<?php
namespace joyqhs\Sdk\aliexpress;
use joyqhs\Sdk\aliexpress\AbstractCallApiUtil;

class CallQmApiUtil extends AbstractCallApiUtil
{
    private $appkey = '24744952';
    private $secretKey = '7fb815f1ac871fb3df8cf6f0a18b8e70';
    private $targetAppkey = "24764211";
    private $gatewayUrl = 'http://tqoem55nju.api.taobao.com/router/qm';

    private $method = 'aliexpress.transfer.station';

    private $format = "json";

    /** 是否打开入参check**/
    protected $signMethod = "md5";

    protected $apiVersion = "2.0";

    protected $sdkVersion = "top-sdk-php-20151012";

    public function callAliexpressApi($method, $sessionKey, $params)
    {
        $postData = $this->buildPostData($method, $sessionKey, $params);
        $gateWayUrl =  $this->gatewayUrl;
        $response = $this->postCurl($gateWayUrl, $postData);
        $responseArr = json_decode($response, true);
        if (isset($responseArr['response']['data'])) {
            $responseArr['response']['data'] = json_decode($responseArr['response']['data'], true, 512, JSON_BIGINT_AS_STRING);
        }
        return isset($responseArr['response']) ? $responseArr['response'] : ['Ack' => 'Failure', 'data' => 'no data'];
    }


    public function buildPostData($method, $sessionKey, $params)
    {
        $postData = [
            'param' => json_encode($this->assembleAliexpressParams($method, $sessionKey, $params))
        ];
        $systemParam = $this->getSystemParams();
        $postData = array_merge($systemParam, $postData);
        $postData['sign'] = $this->generateSign($postData);
        return $postData;
    }

    private function getSystemParams()
    {
        $systemParam = [
            'app_key' => $this->appkey,
            'method' => $this->method,
            'format' => $this->format,
            'target_app_key' => $this->targetAppkey,
            'api_version' => $this->apiVersion,
            'sdk_version' => $this->sdkVersion,
            'sign_method' => $this->signMethod,
            'timestamp' => $this->getTimestamp()
        ];
        return $systemParam;
    }

    private function getTimestamp()
    {
        return date('Y-m-d H:i:s');
    }


    private function generateSign($params)
    {
        ksort($params);
        $stringToBeSigned =  $this->secretKey;
        foreach ($params as $k => $v) {
            if (is_string($v) && "@" != substr($v, 0, 1) && $k !== 'sign') {
                $stringToBeSigned .= "$k$v";
            }
        }
        unset($k, $v);
        $stringToBeSigned .= $this->secretKey;
        return strtoupper(md5($stringToBeSigned));
    }
}
