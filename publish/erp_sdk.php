<?php
declare(strict_types=1);

return [
    'aliexpress' => [
        'appkey' => env('ALI_APPKEY', ''),
        'secret' => env('ALI_SECRET', ''),
    ],
    'ebay' => [
        'appkey' => env('EBAY_APPKEY', ''),
        'secret' => env('EBAY_SECRET', ''),
    ],
    'fop' => [
        'appkey' => env('FOP_APPKEY', ''),
        'secret' => env('FOP_SECRET', ''),
        'appid' => '',
    ],
    'lazada' => [
        'appkey' => env('LAZADA_APPKEY', ''),
        'secret' => env('LAZADA_SECRET', ''),
        'appid' => '',
    ],
    'winit' => [
        'appkey' => env('WINIT_APPKEY', ''),
        'secret' => env('WINIT_SECRET', ''),
        'clientid' => env('WINIT_CLIENT_ID', ''),
        'token' => env('WINIT_TOKEN', ''),
    ],
];
